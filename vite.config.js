import { resolve } from 'path'
import { defineConfig } from 'vite'
import { viteRequire } from 'vite-require'

export default defineConfig({
    resolve: {
      alias: {
        process: "process/browser",
        stream: "stream-browserify",
        zlib: "browserify-zlib",
        util: 'util'
      }
    },
    base: "https://quentinbolsee.pages.cba.mit.edu/gerber2img/",
    build: {
        rollupOptions: {
            input: {
                main: resolve(__dirname, 'index.html'),
                // nested: resolve(__dirname, 'nested/index.html'),
            },
        },
    },
    plugins: [
      viteRequire()
    ]
})
